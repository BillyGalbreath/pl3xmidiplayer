package net.pl3x.pl3xmidiplayer.commands;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import net.pl3x.pl3xmidiplayer.Pl3xMidiPlayer;
import net.pl3x.pl3xmidiplayer.midi.Midi;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdMidi implements CommandExecutor {
	private Pl3xMidiPlayer plugin;
	
	public CmdMidi(Pl3xMidiPlayer plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!cmd.getName().equalsIgnoreCase("midi"))
			return false;
		if (!sender.hasPermission("pl3xmidiplayer.command.midi")) {
			sender.sendMessage(plugin.colorize("&4You do not have permission for this command."));
			return true;
		}
		if (!(sender instanceof Player)) {
			sender.sendMessage(plugin.colorize("&4Cannot use command from console!"));
			return true;
		}
		if (args.length < 1) {
			sender.sendMessage(plugin.colorize("&4Missing parameters!"));
			return false;
		}
		if (args[0].equalsIgnoreCase("list")) {
			//
			return true;
		} else if (args[0].equalsIgnoreCase("play")) {
			if (args.length < 2) {
				sender.sendMessage(plugin.colorize("&4Missing parameters!"));
				return true;
			}
			String fileName = args[1].trim();
			File file = new File(plugin.getDataFolder() + File.separator + "midis" + File.separator + fileName);
			Set<String> playerNames = new HashSet<String>();
			for (Player p : plugin.getServer().getOnlinePlayers()) {
				playerNames.add(p.getName());
			}
			Midi midi = new Midi(file, playerNames);
			midi.start();
			Pl3xMidiPlayer.midis.add(midi);
			sender.sendMessage(plugin.colorize("&dPlaying midi file... ID: " + (Pl3xMidiPlayer.midis.size() - 1)));
			return true;
		} else if (args[0].equalsIgnoreCase("stop")) {
			if (args.length < 2) {
				sender.sendMessage(plugin.colorize("&4Missing parameters!"));
				return true;
			}
			int id = -1;
			try {
				id = Integer.valueOf(args[1]);
			} catch (NumberFormatException npe) {
				sender.sendMessage(plugin.colorize("&4ID must be a number!"));
				return true;
			}
			Midi midi = Pl3xMidiPlayer.midis.get(id);
			if (midi == null) {
				sender.sendMessage(plugin.colorize("&4Midi by that ID is not found!"));
				return true;
			}
			midi.stop();
			sender.sendMessage(plugin.colorize("&dMidi stopped."));
			return true;
		}
		sender.sendMessage(plugin.colorize("&4Unknown subcommand"));
		return true;
	}
}
