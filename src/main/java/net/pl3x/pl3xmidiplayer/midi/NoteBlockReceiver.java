package net.pl3x.pl3xmidiplayer.midi;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import java.io.IOException;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Midi Receiver for processing note events.
 *
 * @author authorblues
 * @modified BillyGalbreath
 */
public class NoteBlockReceiver implements Receiver {
	private static final float VOLUME_RANGE = 10.0f;
	private final Set<String> listeners;
	
	public NoteBlockReceiver(Set<String> listeners) throws InvalidMidiDataException, IOException {
		this.listeners = listeners;
	}
	
	@Override
	public void send(MidiMessage m, long time) {
		if (!(m instanceof ShortMessage))
			return;
		ShortMessage smessage = (ShortMessage) m;
		switch (smessage.getCommand()) {
		case ShortMessage.NOTE_ON:
			this.playNote(smessage);
			break;
		case ShortMessage.NOTE_OFF:
			break;
		}
	}
	
	public void playNote(ShortMessage message) {
		// if this isn't a NOTE_ON message, we can't play it
		if (ShortMessage.NOTE_ON != message.getCommand())
			return;
		
		// get pitch and volume from the midi message
		float pitch = (float) ToneUtil.midiToPitch(message);
		float volume = VOLUME_RANGE * ((float) message.getData2() / 127.0f);
		
		for (String name : listeners) {
			Player player = Bukkit.getServer().getPlayerExact(name);
			if (player == null)
				return;
			player.playSound(player.getLocation(), Sound.NOTE_PIANO, volume, pitch);
		}
	}
	
	@Override
	public void close() {
		listeners.clear();
	}
	
	public void addListeners(Set<String> players) {
		for (String player : players)
			if (!isListener(player))
				listeners.add(player);
	}
	
	public void addListener(String player) {
		if (!isListener(player))
			listeners.add(player);
	}
	
	public void removeListeners(Set<String> players) {
		for (String player : players)
			if (isListener(player))
				listeners.remove(player);
	}
	
	public void removeListener(String player) {
		if (isListener(player))
			listeners.remove(player);
	}
	
	public boolean isListener(String player) {
		if (listeners.contains(player))
			return true;
		return false;
	}
}
