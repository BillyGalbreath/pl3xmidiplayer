package net.pl3x.pl3xmidiplayer.midi;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;

public class Midi {
	private Sequencer sequencer;
	private NoteBlockReceiver noteBlockReceivers;
	
	private void setupMidiFile(File file, float tempo, Set<String> listeners) throws MidiUnavailableException, InvalidMidiDataException, IOException {
		noteBlockReceivers = new NoteBlockReceiver(listeners);
		sequencer = MidiSystem.getSequencer(false);
		sequencer.setSequence(MidiSystem.getSequence(file));
		sequencer.open();
		sequencer.setTempoFactor(tempo);
		sequencer.getTransmitter().setReceiver(noteBlockReceivers);
	}
	
	private void construct(File file, float tempo, Set<String> listeners) {
		try {
			setupMidiFile(file, tempo, listeners);
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Midi(File file, float tempo, Set<String> listeners) {
		construct(file, tempo, listeners);
	}
	
	public Midi(File file, Set<String> listeners) {
		construct(file, 1.0F, listeners);
	}
	
	public boolean start() {
		if (sequencer == null)
			return false;
		sequencer.start();
		return true;
	}
	
	public boolean stop() {
		if (sequencer == null)
			return false;
		sequencer.stop();
		return true;
	}
	
	public long getLength() {
		return sequencer.getTickLength();
	}
	
	public long getPosition() {
		return sequencer.getTickPosition();
	}
	
	public void setLoop(int count) {
		sequencer.setLoopStartPoint(0);
		sequencer.setLoopEndPoint(sequencer.getTickLength());
		if (count < 0)
			sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
		sequencer.setLoopCount(count);
	}
	
	public void addListeners(Set<String> players) {
		noteBlockReceivers.addListeners(players);
	}
	
	public void addListener(String player) {
		noteBlockReceivers.addListener(player);
	}
	
	public void removeListeners(Set<String> players) {
		noteBlockReceivers.removeListeners(players);
	}
	
	public void removeListener(String player) {
		noteBlockReceivers.removeListener(player);
	}
	
	public boolean isListener(String player) {
		return noteBlockReceivers.isListener(player);
	}
}
