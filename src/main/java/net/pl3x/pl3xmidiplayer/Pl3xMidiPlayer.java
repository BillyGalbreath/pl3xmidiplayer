package net.pl3x.pl3xmidiplayer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import net.pl3x.pl3xmidiplayer.commands.CmdMidi;
import net.pl3x.pl3xmidiplayer.midi.Midi;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

public class Pl3xMidiPlayer extends JavaPlugin {
	public static List<Midi> midis = new ArrayList<Midi>();
	
	public void onEnable() {
		if (!new File(getDataFolder() + File.separator + "config.yml").exists())
			saveDefaultConfig();
		
		//Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
		
		getCommand("midi").setExecutor(new CmdMidi(this));
		
		setupMetrics();
		
		log(getName() + " v" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		Bukkit.getLogger().setFilter(null);
		log(getName() + " Disabled.");
	}
	
	public void log (Object obj) {
		if (getConfig().getBoolean("color-logs", true)) {
			getServer().getConsoleSender().sendMessage(colorize("&3[&d" +  getName() + "&3]&r " + obj));
		} else {
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
		}
	}
	
	public String colorize(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}
	
	private void setupMetrics() {
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			log("&4Failed to start Metrics: &e" + e.getMessage());
		}
	}
}
